	var width = window.innerWidth;

//XML Data Parsing Activity
function xmlParse(function_name, method, xml_address) {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
     memberData(this, true)
    }
  };
  xmlhttp.open(method, xml_address, true);
  xmlhttp.send();
}

function memberData(xml, jsEnable) {
var data = "<!--Loaded data via ajax-->";
var xmlDoc = xml.responseXML;
var tagName = xmlDoc.getElementsByTagName("MEMBER");
var loadText = "";
if(width<=768){loadText = "dev/";}
else if(width>=768){loadText = "loads/dev/";}
	for (var i = 0; i <tagName.length; i++) {
data += "<div class=\"dialog-t-btn p-item\" title=\""+ tagName[i].getElementsByTagName("NAME")[0].childNodes[0].nodeValue +"\">"+
"<div class=\"p-photo-wrapper \"><div class=\"p-photo p-i-p-"+ tagName[i].getElementsByTagName("CODENAME")[0].childNodes[0].nodeValue +"\"></div></div>\n"+
"<span class=\"name\">"+ tagName[i].getElementsByTagName("NAME")[0].childNodes[0].nodeValue +"</span>\n"+
"<div class=\"p-i-position\">"+tagName[i].getElementsByTagName("POSITION")[0].childNodes[0].nodeValue +"</div>\n"+
"</div>\n"+
			"<div class=\"dialog-t-contents\">"+
				 "<div class=\"dialog-i-header\">"+
					"<div class=\"p-i-label geom-icon-rev\">"+
						"<div class=\"p-s p-i-p-"+ tagName[i].getElementsByTagName("CODENAME")[0].childNodes[0].nodeValue +"\"></div>"+
						"<span class=\"name namex\">"+ tagName[i].getElementsByTagName("NAME")[0].childNodes[0].nodeValue +"</span>"+
						"<div class=\"p-i-position p-scs\">"+tagName[i].getElementsByTagName("POSITION")[0].childNodes[0].nodeValue +"</div>"+
					"<div class=\"social soc-l\"><ul>"+
						"<li>"+
							"<a href=\"#\" class=\"fa fa-google-plus\" title=\"Google Plus\"></a>"+
						"</li>"+
						"<li>"+
							"<a href=\"#\" class=\"fa fa-twitter\" title=\"Twitter\"></a>"+
						"</li>"+
						"<li>"+
							"<a href=\"#\" class=\"fa fa-facebook\" title=\"Facebook\"></a>"+
						"</li>"+
					"</ul></div>"+
					"</div>"+
				"</div>"+
			"<div class=\"d-c-d dialog-i-content-"+ tagName[i].getElementsByTagName("CODENAME")[0].childNodes[0].nodeValue +"\"></div>"+
		"</div>";
}
	$('div.portfolio-wrapper').append(data);
	$('div.portfolio-wrapper').append("<script src=\"../../js/platform.js\"></script>");
	for (var i = 0; i <tagName.length; i++) {
		networkFetch("POST","div.dialog-i-content-"+ tagName[i].getElementsByTagName("CODENAME")[0].childNodes[0].nodeValue,loadText+tagName[i].getElementsByTagName("CODENAME")[0].childNodes[0].nodeValue+webExt,false);
	}
 }

xmlParse("memberData","GET","../xml/team.xml");

	var mainCSS = document.getElementById("mainCSS");
	var mainTag = document.getElementsByTagName("BODY")[0];
	var mobileCSS = "../css/"+"mobile.css";
	var webCSS = "../css/"+"web.css";
	var width = window.innerWidth;
	var webExt = ".html";

//Verify the client browser screen weather what type of CSS will be sent or use
try {
	if(width<=768){mainCSS.setAttribute("href", mobileCSS);}
	if(width>=768){mainCSS.setAttribute("href", webCSS);}
}catch(err){}
var objectTeamLoad = document.getElementsByClassName("team-x")[0];
var teamPW = document.getElementsByClassName("portfolio-wrapper")[0];
var teamC = document.getElementsByClassName("cards")[0];
var load = document.getElementsByClassName("loader")[0];
	setTimeout(function(){ teamC.classList.add("cards-show"); }, 1000);
	setTimeout(function(){ teamPW.classList.add("portfolio-show"); }, 1000);
