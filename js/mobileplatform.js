var mainTag = document.getElementsByTagName("MAIN")[0];
var mainHeader = document.getElementsByTagName("HEADER")[0];
var bodyTag = document.getElementsByTagName("BODY")[0];
var mainItem = document.getElementsByClassName("dialog-btn");
var mainBTNS = document.getElementsByClassName("btn-scroll-top")[0];
var sb = document.getElementsByClassName("sb")[0];
var mainTitle = document.getElementsByTagName("TITLE")[0];
var titleTagTemp="";
var titleTagTemp2="";

var ir=0;
var ib=0;
var nb=0;

var teamCrI = false;
var signinCrI = false;
var portfolioCrI = false;

var teamL = false;
var signinL = false;
var portfolioL = false;

function background(){
	ib++;
	bodyTag.classList.add("b"+ib);
	if(ib>0){
		nb=ib-1;
		bodyTag.classList.remove("b"+nb);
	}
	if(ib>=8){
	bodyTag.classList.remove("b"+ib);
	ib=0;
	bodyTag.classList.add("b"+(ib+1));
	}
	setTimeout(background, 5000);
}

//Toggle Search
	var searchButton = document.getElementsByClassName("search-btn");
	var searchDiv = document.getElementsByClassName("sc-search")[0];
	for(var a=0; a<searchButton.length; a++){
		searchButton[a].onclick = function(){
		this.classList.toggle("search-i-toggle");
		searchDiv.classList.toggle("sc-seach-show");
		}
	}

//Toggle Nav
	var navButton = document.getElementsByClassName("sc-menu-btn");
	var header = document.getElementsByTagName("header")[0];
	var navButton2 = document.getElementsByClassName("sc-menu-btn")[0];
	var navShell = document.getElementsByClassName("app-shell")[0];
	for(var a=0; a<navButton.length; a++){
		navButton[a].onclick = function(){
		if ((navShell.classList.contains("nav-show"))
		&&(navShell.classList.contains("nav-show"))) {
			navButton2.classList.toggle("bar-click");
		}else{
			navButton2.classList.toggle("bar-click");
		}
		navShell.classList.toggle("nav-show");
		header.classList.toggle("h");
		}
	}

//Toggle Footer Accordion
	var snackButton0 = document.getElementsByClassName("f-label");
	for(var a=0; a<snackButton0.length; a++){
		snackButton0[a].onclick = function(){
		this.classList.toggle("f-l");
		this.nextElementSibling.classList.toggle("show-s");
		}
	}

	var sNavigationTint = document.getElementsByClassName("sc-head")[0];
	mainTag.onscroll=function(){
			if(mainTag.scrollTop>50){
			sNavigationTint.classList.add("sc-scroll");
				mainHeader.classList.add("head-shadow");
		}else{
			sNavigationTint.classList.remove("sc-scroll");
			mainHeader.classList.remove("head-shadow");
		}
		if(mainTag.scrollTop>180){
			rotatefade();
			mainBTNS.classList.add("btn-scroll-show");
			mainBTNS.classList.add("animatetop");
		}else{
			mainBTNS.classList.remove("btn-scroll-show");
			mainBTNS.classList.remove("animatetop");
		}
	}

	//Dialog Toggler
	var dbtn = document.getElementsByClassName("dialog-btn");
	var appshell = document.getElementsByClassName("app-shell")[0];
	var glass = document.getElementsByClassName("glass")[0];
	var s;
	for (var i = 0; i < dbtn.length; i++) {
		dbtn[i].onclick = function(){
			this.classList.toggle("dialog-circ");
			this.nextElementSibling.classList.toggle("dialog-toggle");
			appshell.classList.toggle("app-hide");
			glass.classList.toggle("glass-wrapper");
		}
	}
	//Toggle Sign In Window
		var signin = document.getElementsByClassName("sign");
		var signinDialog = document.getElementById("signin");
		for(var a=0; a<signin.length; a++){
			signin[a].onclick = function(){
			signinDialog.classList.toggle("signin-show");
			appshell.classList.toggle("app-hide");
			if ((signinDialog.classList.contains("signin-show"))
			&&(appshell.classList.contains("app-hide"))) {
				titleTagTemp = mainTitle.innerHTML;
				mainTitle.innerHTML = "Sign In : Survs";
				signinCreate();
			}else{
				mainTitle.innerHTML = titleTagTemp;
			}
			}
		}
	//Toggle Team In Window
		var team = document.getElementsByClassName("team");
		var teamDialog = document.getElementById("team");
		for(var a=0; a<team.length; a++){
			team[a].onclick = function(){
			teamDialog.classList.toggle("team-show");
			appshell.classList.toggle("app-hide");
			if ((teamDialog.classList.contains("team-show"))
			&&(appshell.classList.contains("app-hide"))) {
				titleTagTemp = mainTitle.innerHTML;
				mainTitle.innerHTML = "Our Amazing Team : Survs";
				teamCreate();
			}else{
				mainTitle.innerHTML = titleTagTemp;
			}
			}
		}
	//Toggle Portfolio In Window
		var portfolio= document.getElementsByClassName("portfolio");
		var portfolioDialog = document.getElementById("portfolio");
		for(var a=0; a<portfolio.length; a++){
			portfolio[a].onclick = function(){
			portfolioDialog.classList.toggle("portfolio-show");
			appshell.classList.toggle("app-hide");
			if ((portfolioDialog.classList.contains("portfolio-show"))
			&&(appshell.classList.contains("app-hide"))) {
				titleTagTemp = mainTitle.innerHTML;
				mainTitle.innerHTML = "Our Portfolio' : Survs";
				portfolioCreate();
				//galerrySlide()
			}else{
				mainTitle.innerHTML = titleTagTemp;
			}
			}
		}
	//Toggle Form Label
		var formLabel = document.getElementsByClassName("form-label");
		for(var a=0; a<formLabel.length; a++){
			formLabel[a].onclick = function(){
			this.classList.toggle("form-label-in");
			}
		}
	//Dialog Portfolio Toggler
	var dbtnp = document.getElementsByClassName("dialog-btn-p");
	var appshell = document.getElementsByClassName("app-shell")[0];
	//var tm = document.getElementsByClassName("t-m")[0];
	for (var i = 0; i < dbtnp.length; i++) {
		dbtnp[i].onclick = function(){
			this.classList.toggle("dialog-active-p");
			this.nextElementSibling.classList.toggle("dialog-toggle-p");
			appshell.classList.toggle("app-hide");
			//tm.classList.toggle("team-o");
		}
		//dbtnp[i].onclick = galerrySlide();
	}
	function rotatefade(){
		try{
			if(ir>mainItem.length){
			ir=0;
			}if(ir<mainItem.length){
			mainItem[ir].classList.add("show-div");
			mainItem[ir].classList.add("animatetop");
			ir++;
		}setTimeout(rotatefade, 200);
		}catch(err){}
	}

	function teamCreate() {
		if(!teamCrI){
			var teamC = document.createElement("IFRAME");
			var teamP = document.getElementsByClassName("t-m")[0];
			teamC.setAttribute("src", "loads/team.html");
			teamC.setAttribute("class", "team-object");
			teamP.appendChild(teamC);
			teamCrI=true;
		}
		if(teamCrI){
			if(!teamL){
				snackBarDialogCreator("loader","team","Loading Contents...",0, true);
			}
		}
	}
	function signinCreate() {
		if(!signinCrI){
			var signinC = document.createElement("IFRAME");
			var signinP = document.getElementsByClassName("s-m")[0];
			signinC.setAttribute("src", "loads/signin.html");
			signinC.setAttribute("class", "signin-object");
			signinP.appendChild(signinC);
			signinCrI=true;
		}
		if(signinCrI){
			if(!signinL){
				snackBarDialogCreator("loader","signin","Loading Contents...",0, true);
			}
		}
	}
	function portfolioCreate() {
		if(!portfolioCrI){
			var portfolioC = document.createElement("IFRAME");
			var portfolioP = document.getElementsByClassName("p-m")[0];
			portfolioC.setAttribute("src", "loads/portfolio.html");
			portfolioC.setAttribute("class", "portfolio-object");
			portfolioP.appendChild(portfolioC);
			portfolioCrI=true;
		}
		if(portfolioCrI){
			if(!portfolioL){
				snackBarDialogCreator("loader","portfolio","Loading Contents...",0, true);
			}
		}
	}

	function snackBarDialogCreator(req_type,referer,text,seconds,icon){
			var snackbar = document.createElement("DIV");
			snackbar.setAttribute("class", "snackbar-"+req_type);
			snackbar.setAttribute("id", "snackbar");
			sb.appendChild(snackbar);
			if(icon){
				var iconImage = document.createElement("DIV");
				iconImage.setAttribute("class", "image-"+req_type);
				document.getElementsByClassName("snackbar-"+req_type)[0].appendChild(iconImage);
			}
			var snackbarText = document.createElement("DIV");
			snackbarText.setAttribute("class", "snackbarText");
			document.getElementsByClassName("snackbar-"+req_type)[0].appendChild(snackbarText);
			document.getElementsByClassName("snackbarText")[0].innerHTML = text;

			var snackbarDiv = document.getElementById("snackbar");
			sb.classList.add("sb-t");
			snackbarDiv.classList.add("show");
			if(req_type=="loader"){
				var spinner = document.createElement("DIV");
				spinner.setAttribute("class", "spinner");
				document.getElementsByClassName("snackbar-"+req_type)[0].appendChild(spinner);

				var d1 = document.createElement("d1");
				d1.setAttribute("class", "d1");
				document.getElementsByClassName("spinner")[0].appendChild(d1);

				var d2 = document.createElement("d2");
				d2.setAttribute("class", "d2");
				document.getElementsByClassName("spinner")[0].appendChild(d2);

				if(referer=="team"){
						var teamObject = document.getElementsByClassName("team-object")[0];
						teamObject.onload = function(){
							snackbarDiv.classList.remove("show");
							sb.classList.remove("sb-t");
							sb.removeChild(sb.childNodes[0]);
							teamL=true;
						}
				}
				if(referer=="signin"){
						var signinObject = document.getElementsByClassName("signin-object")[0];
						signinObject.onload = function(){
							snackbarDiv.classList.remove("show");
							sb.classList.remove("sb-t");
							sb.removeChild(sb.childNodes[0]);
							signinL=true;
						}
				}
				if(referer=="portfolio"){
						var portfolioObject = document.getElementsByClassName("portfolio-object")[0];
						portfolioObject.onload = function(){
							snackbarDiv.classList.remove("show");
							sb.classList.remove("sb-t");
							sb.removeChild(sb.childNodes[0]);
							portfolioL=true;
						}
				}
			}
			if(req_type!="loader"){
				setTimeout(function(){
					snackbarDiv.classList.remove("show");
				}, (seconds*1000));
				setTimeout(function(){
					sb.classList.remove("sb-t");
					sb.removeChild(sb.childNodes[0]);
				}, ((seconds+1)*1000));
			}
	}
	background();
