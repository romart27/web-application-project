// Created by: Romart Mediante
// Delivering content responsively
	var mainCSS = document.getElementById("mainCSS");
	var mainTag = document.getElementsByTagName("BODY")[0];
	var mainFlash = document.getElementsByClassName("flashscreen")[0];
	var c = ".css";
	var cs = "css/";
	var link1 = "<link rel=\"stylesheet\" href=\"";
	var link2 = "\"/>";
	var mobile = cs+"mobile"+c;
	var web = cs+"web"+c;
	var width = window.innerWidth;
	var vwidth = screen.width;
	var ver1 = false;

//Verify the client browser screen weather what type of CSS will be sent or use
try {
	if(width<=768){mainCSS.setAttribute("href", mobile);ver1=true;}
	else if(width>=768){mainCSS.setAttribute("href", web);}
}catch(err){}

function flashscreenStarter() {
	mainFlash.classList.add("f-loaded");
}

//Unloading Main Body Activity
mainTag.onload=function(){
	$("head").append(link1+cs+"media"+c+link2);
	sections();
	$("head").append(link1+cs+"lato"+c+link2);
};

function sections(){
	$("main").append("<section id=\"home\" class=\"surv-p\"></section>");
	$("main").append("<section id=\"services\" class=\"show\"></section>");
	$("main").append("<section id=\"about\" class=\"show mountain-icon \"><div class=\"section-wrapper\">"+"<div class=\"label-sec\"><span>About</span></div><div class=\"section-margin\"><div class=\"about-content\"></div></div></div></section>");
	$("main").append("<section id=\"partners\" class=\"show geom-icon\"></section>");
	$("main").append("<footer></footer>");
	$("nav").load("loads/navContent.data");
	$("footer").load("loads/footerContent.data");
	$("#home").load("loads/home.html");
	$("#services").load("loads/services.html");
	$("#partners").load("loads/partners.html");
	$("div.about-content").load("strings/about.txt");
	$("div.dialog-activities").load("loads/dialogActivities.data");
	scriptExecution();
}

function scriptExecution() {
	if(ver1==false){
		$("head").append("<title>Survs Company : Web</title>");
		$("#script").load("js/web.img");
			setTimeout(flashscreenStarter, 3000);
	}
	if(ver1==true){
		$("head").append("<title>Survs Company : Mobile</title>");
		$("#script").load("js/mobile.img");
			setTimeout(flashscreenStarter, 3000);
	}
}
