var mainTag = document.getElementsByTagName("BODY")[0];
var mainTitle = document.getElementsByTagName("TITLE")[0];
var mainDataTag = document.getElementsByTagName("MAIN")[0];
var mainHeader = document.getElementsByTagName("HEADER")[0];
var mainNav = document.getElementsByTagName("NAV")[0];
var mainBlur = document.getElementsByClassName("blur")[0];
var mainPoly = document.getElementsByClassName("poly")[0];
var mainBTNS = document.getElementsByClassName("btn-scroll-top")[0];
var mainItem = document.getElementsByClassName("dialog-btn");
var galleryDP = document.getElementsByClassName("dialog-btn-p");
var ib=0;
var nb=0;
var ir=0;
var is=0;
var loadText = "loads/";
var webExt = ".html";
var titleTagTemp="";
var titleTagTemp2="";

function background(){
	ib++;
	mainTag.classList.add("b"+ib);
	mainBlur.classList.add("b"+ib);
	mainPoly.classList.add("b"+ib);
	if(ib>0){
		nb=ib-1;
		mainTag.classList.remove("b"+nb);
		mainBlur.classList.remove("b"+nb);
		mainPoly.classList.remove("b"+nb);
	}
	if(ib>=8){
	mainTag.classList.remove("b"+ib);
	mainBlur.classList.remove("b"+ib);
	mainPoly.classList.remove("b"+ib);
	ib=0;
	mainTag.classList.add("b"+(ib+1));
	mainBlur.classList.add("b"+(ib+1));
	mainPoly.classList.add("b"+(ib+1));
	}
	setTimeout(background, 5000);
}
mainDataTag.onscroll=function(){
    if(mainDataTag.scrollTop>50){
		mainHeader.classList.remove("header-top");
		mainNav.classList.remove("nav-top");
	}else{
		mainHeader.classList.add("header-top");
		mainNav.classList.add("nav-top");
	}
	if(mainDataTag.scrollTop>180){
		rotatefade();
		mainBTNS.classList.add("btn-scroll-show");
		mainBTNS.classList.add("animatetop");
	}else{
		mainBTNS.classList.remove("btn-scroll-show");
		mainBTNS.classList.remove("animatetop");
	}
}
function rotatefade(){
	try{
		if(ir>mainItem.length){
		ir=0;
		}if(ir<mainItem.length){
		mainItem[ir].classList.add("show-div");
		mainItem[ir].classList.add("animatetop");
		ir++;
		}setTimeout(rotatefade, 500);
	}catch(err){}
}

function galerrySlide(){
		if(is>galleryDP.length){
		is=0;
		}if(is<galleryDP.length){
		galleryDP[is].classList.add("show");
		galleryDP[is].classList.add("d-p-i");
		is++;
		}setTimeout(galerrySlide, 500);
}
//Network Fetch/Recieve Activity
function networkFetch(method,selector,filename, jsEnable){
	var fetch = new XMLHttpRequest();
	fetch.onreadystatechange = function() {
		if(this.readyState == 4 && this.status==404){
			document.getElementById(selector).innerHTML = "<div id=\"snackbar\" class=\"snackbar-wrapper\"><div class=\"snackbar"+" error\">Error Loading ["+filename+"]</div></div>";
			var snackbar = document.getElementById("snackbar");
			snackbar.className = "show";
			setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 5000);
		}
		if(this.readyState == 4 && this.status == 200){
			$(selector).append(this.responseText);
			if(jsEnable==true){
				$(selector).append("<script src=\"js/platform.js\"></script>\n<script src=\"js/jquery.min.js\"></script>");
			}
		}
	};
	fetch.open(method, filename, true);
	fetch.send();
}

//Accordion on Footer
	var snackButton0 = document.getElementsByClassName("f-label");
	for(var a=0; a<snackButton0.length; a++){
		snackButton0[a].onclick = function(){
		this.nextElementSibling.classList.toggle("show");
		this.classList.toggle("c");
		}
	}
//Unloading Main Body Activity
//	try{
		background();
		networkFetch("POST","#signin",loadText+"signin"+webExt,true);
		networkFetch("POST","#team",loadText+"team-w"+webExt,false);
		networkFetch("POST","#portfolio",loadText+"portfolio"+webExt,false);
		networkFetch("POST","div.about-content","strings/about.txt", false);
		networkFetch("POST","div.dialog-content","strings/about.txt", false);
//	}catch(err){}
