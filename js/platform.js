var titleTagTemp="";
var titleTagTemp2="";
var mainTitle = document.getElementsByTagName("TITLE")[0];

//Dialog Toggler
var dbtn = document.getElementsByClassName("dialog-btn");
var appshell = document.getElementsByClassName("app-shell")[0];
var glass = document.getElementsByClassName("glass")[0];
var s;
for (var i = 0; i < dbtn.length; i++) {
	dbtn[i].onclick = function(){
		this.classList.toggle("dialog-circ");
		this.nextElementSibling.classList.toggle("dialog-toggle");
		appshell.classList.toggle("app-hide");
		glass.classList.toggle("glass-wrapper");
	}
}
//Toggle Sign In Window
	var signin = document.getElementsByClassName("sign");
	var signinDialog = document.getElementById("signin");
	for(var a=0; a<signin.length; a++){
		signin[a].onclick = function(){
		signinDialog.classList.toggle("signin-show");
		appshell.classList.toggle("app-hide");
		mainHeader.classList.toggle("blur");
		mainDataTag.classList.toggle("blur");
		if ((signinDialog.classList.contains("signin-show"))
		&&(appshell.classList.contains("app-hide"))) {
			titleTagTemp = mainTitle.innerHTML;
			mainTitle.innerHTML = "Sign In : Survs";
		}else{
			mainTitle.innerHTML = titleTagTemp;
		}
		}
	}
//Toggle Team In Window
	var team = document.getElementsByClassName("team");
	var teamDialog = document.getElementById("team");
	for(var a=0; a<team.length; a++){
		team[a].onclick = function(){
		teamDialog.classList.toggle("team-show");
		appshell.classList.toggle("app-hide");
		mainHeader.classList.toggle("blur");
		mainDataTag.classList.toggle("blur");
		if ((teamDialog.classList.contains("team-show"))
		&&(appshell.classList.contains("app-hide"))) {
			titleTagTemp = mainTitle.innerHTML;
			mainTitle.innerHTML = "Our Amazing Team : Survs";
		}else{
			mainTitle.innerHTML = titleTagTemp;
		}
		}
	}
//Toggle Portfolio In Window
	var portfolio= document.getElementsByClassName("portfolio");
	var portfolioDialog = document.getElementById("portfolio");
	for(var a=0; a<portfolio.length; a++){
		portfolio[a].onclick = function(){
		portfolioDialog.classList.toggle("portfolio-show");
		appshell.classList.toggle("app-hide");
		mainHeader.classList.toggle("blur");
		mainDataTag.classList.toggle("blur");
		if ((portfolioDialog.classList.contains("portfolio-show"))
		&&(appshell.classList.contains("app-hide"))) {
			titleTagTemp = mainTitle.innerHTML;
			mainTitle.innerHTML = "Our Portfolio' : Survs";
			galerrySlide()
		}else{
			mainTitle.innerHTML = titleTagTemp;
		}
		}
	}
//Toggle Form Label
	var formLabel = document.getElementsByClassName("form-label");
	for(var a=0; a<formLabel.length; a++){
		formLabel[a].onclick = function(){
		this.classList.toggle("form-label-in");
		}
	}
//Dialog Team Toggler
var dbtnt = document.getElementsByClassName("dialog-t-btn");
var appshell = document.getElementsByClassName("app-shell")[0];
var tm = document.getElementsByClassName("t-m")[0];
for (var i = 0; i < dbtnt.length; i++) {
	dbtnt[i].onclick = function(){
		this.classList.toggle("dialog-t-active");
		this.nextElementSibling.classList.toggle("dialog-t-toggle");
		/*tm.classList.toggle("team-o");
		if ((this.classList.contains("dialog-t-active"))
		&&(this.nextElementSibling.classList.contains("dialog-t-toggle"))) {
			titleTagTemp2 = mainTitle.innerHTML;
			mainTitle.innerHTML = this.title+"'s Profile";
		}else{
			mainTitle.innerHTML = titleTagTemp2;
		} */
	}
}
//Dialog Portfolio Toggler
var dbtnp = document.getElementsByClassName("dialog-btn-p");
var appshell = document.getElementsByClassName("app-shell")[0];
//var tm = document.getElementsByClassName("t-m")[0];
for (var i = 0; i < dbtnp.length; i++) {
	dbtnp[i].onclick = function(){
		this.classList.toggle("dialog-active-p");
		this.nextElementSibling.classList.toggle("dialog-toggle-p");
		appshell.classList.toggle("app-hide");
		//tm.classList.toggle("team-o");
	}
	//dbtnp[i].onclick = galerrySlide();
}

var formW = document.getElementsByClassName("s-help");
var fw = document.getElementsByClassName("form-wrapper")[0];
for (var i = 0; i < formW.length; i++) {
	formW[i].onclick = function(){
		this.classList.toggle("s-h");
		fw.classList.toggle("f-w");
	}
}
//Mobile Nav Shrink
if(window.innerWidth<769){
	var pL = document.getElementsByClassName("p-i-label");
	for(var i=0; i<pL.length; i++){
	var dialogToggleSc = document.getElementsByClassName("dialog-t-contents");
	var pLabel = document.getElementsByClassName("p-i-label");
	var ps = document.getElementsByClassName("p-s");
	var namex = document.getElementsByClassName("namex");
	var pcs = document.getElementsByClassName("p-scs");
	var soc = document.getElementsByClassName("soc-l");
	var dcd = document.getElementsByClassName("d-c-d");
		dialogToggleSc[i].onscroll=function(){
			for(var n =0; n<pLabel.length; n++){
					if(dialogToggleSc[n].scrollTop>10){
							pLabel[n].classList.add("p-sc-label");
							ps[n].classList.add("p-sc");
						  namex[n].classList.add("name-sc");
							pcs[n].classList.add("pp-sc");
							soc[n].classList.add("so-l");
							dcd[n].classList.add("d-sc-d");
					}else{
							pLabel[n].classList.remove("p-sc-label");
							ps[n].classList.remove("p-sc");
							namex[n].classList.remove("name-sc");
							pcs[n].classList.remove("pp-sc");
							soc[n].classList.remove("so-l");
							dcd[n].classList.remove("d-sc-d");
						}
			}
		}
		console.log("d"+i);
	}
}
