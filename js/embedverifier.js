// Created by: Romart Mediante
// Delivering content responsively
	var mainCSS = document.getElementById("mainCSS");
	var mainTag = document.getElementsByTagName("BODY")[0];
	var c = ".css";
	var cs = "../css/";
	var link1 = "<link rel=\"stylesheet\" href=\"";
	var link2 = "\"/>";
	var mobile = cs+"mobile"+c;
	var web = cs+"web"+c;
	var width = window.innerWidth;
	var vwidth = screen.width;
	var ver1 = false;

//Verify the client browser screen weather what type of CSS will be sent or use
try {
	if(width<=768){mainCSS.setAttribute("href", mobile);ver1=true;}
	else if(width>=768){mainCSS.setAttribute("href", web);}
}catch(err){}

//Unloading Main Body Activity
mainTag.onload=function(){
	$("head").append(link1+cs+"lato"+c+link2);
	$("head").append(link1+cs+"media"+c+link2);
	scriptExecution();
};


function scriptExecution() {
	if(ver1==false){
		$("head").append("<title>Survs Company : Web</title>");
		$("#script").load("js/web.img");
	}
	if(ver1==true){
		$("head").append("<title>Survs Company : Mobile</title>");
		$("#script").load("js/mobile.img");
	}
}

//Network Fetch/Recieve Activity
function networkFetch(method,selector,filename, jsEnable){
	var fetch = new XMLHttpRequest();
	fetch.onreadystatechange = function() {
		if(this.readyState == 4 && this.status==404){
			document.getElementById(selector).innerHTML = "<div id=\"snackbar\" class=\"snackbar-wrapper\"><div class=\"snackbar"+" error\">Error Loading ["+filename+"]</div></div>";
			var snackbar = document.getElementById("snackbar");
			snackbar.className = "show";
			setTimeout(function(){ snackbar.className = snackbar.className.replace("show", ""); }, 5000);
		}
		if(this.readyState == 4 && this.status == 200){
			$(selector).append(this.responseText);
			if(jsEnable==true){
				$(selector).append("<script src=\"js/platform.js\"></script>\n<script src=\"js/jquery.min.js\"></script>");
			}
		}
	};
	fetch.open(method, filename, true);
	fetch.send();
}
